<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JpbonusAward */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jpbonus-award-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jackpot_event_id')->textInput() ?>

    <?= $form->field($model, 'total_games_correct')->textInput() ?>

    <?= $form->field($model, 'jackpot_bonus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'betika_points_bonus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'created')->textInput() ?>

    <?php //$form->field($model, 'modified')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
