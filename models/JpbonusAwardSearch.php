<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JpbonusAward;

/**
 * JpbonusAwardSearch represents the model behind the search form of `app\models\JpbonusAward`.
 */
class JpbonusAwardSearch extends JpbonusAward
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jackpot_event_id', 'total_games_correct','status'], 'integer'],
            [['jackpot_bonus', 'betika_points_bonus'], 'number'],
            [['created_by','approved_by', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JpbonusAward::find();

        // Display only records not approved
        //$query = JpbonusAward::find()->where(['status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jackpot_event_id' => $this->jackpot_event_id,
            'total_games_correct' => $this->total_games_correct,
            'jackpot_bonus' => $this->jackpot_bonus,
            'betika_points_bonus' => $this->betika_points_bonus,
            //'created_by' => $this->created_by,
            //'approved_by' => $this->approved_by,
            'status' => $this->status,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by]);
        $query->andFilterWhere(['like', 'approved_by', $this->approved_by]);

        return $dataProvider;
    }
}
